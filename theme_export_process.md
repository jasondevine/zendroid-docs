# 1. FIND AND REPLACE 

Until such time as all URL's in the theme files are root-relative using `<?php get_template_uri(); ?>`then you need to run a 'find and replace' in SublimeText on the home URL of the site you're importing from, converting it to the home URL of the new site the theme is being enabled on. Make sure that you have the correct directory added (this should be the root directory of the theme)

### Example:
	
![Find and replace in Sublime Text 3](https://bytebucket.org/jasondevine/zendroid-docs/raw/b9f936ae886da616b1f5b4dc2a679382e833cb4f/screenshots/sublime_text_find_and_replace.png)


